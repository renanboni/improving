package com.boni.improving.features.issues

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import com.boni.improving.data.base.BaseViewModel
import com.boni.improving.data.base.ErrorState
import com.boni.improving.data.base.Result
import com.boni.improving.data.base.ViewState
import com.boni.improving.data.repository.ImprovingRepository
import com.boni.improving.features.model.Issue
import com.boni.improving.utils.toIssueList

class IssuesViewModel(private val issuesRepository: ImprovingRepository) : BaseViewModel() {

    private val issueState = intoMediator<IssuesState.IssueList>()

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun doIssuesRequest() {
        load {
            issuesRepository.getIssues().let {
                if (it is Result.Success) {
                    issueState.postValue(IssuesState.IssueList(it.data.toIssueList()))
                } else {
                    val error = (it as Result.Error).exception.localizedMessage
                    errorLiveData.value = ErrorState(error)
                }
            }
        }
    }
}

sealed class IssuesState : ViewState {
    class IssueList(val issues: List<Issue>) : IssuesState()
}