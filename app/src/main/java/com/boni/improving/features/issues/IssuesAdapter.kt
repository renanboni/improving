package com.boni.improving.features.issues

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.boni.improving.R
import com.boni.improving.features.model.Issue
import kotlinx.android.synthetic.main.issue_item_list.view.*

class IssuesAdapter(
    private val issueList: List<Issue>,
    private val listener: (Issue) -> Unit
) : RecyclerView.Adapter<IssuesAdapter.IssuesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IssuesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.issue_item_list,
            parent,
            false
        )

        return IssuesViewHolder(view)
    }

    override fun getItemCount() = issueList.count()

    override fun onBindViewHolder(holder: IssuesViewHolder, position: Int) {
        holder.bind(issueList[position])
    }

    inner class IssuesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val container: LinearLayout = view.container
        private val title: TextView = view.title
        private val state: TextView = view.state

        fun bind(issue: Issue) {
            title.text = issue.title
            state.text = issue.state

            container.setOnClickListener { listener(issue) }
        }
    }
}