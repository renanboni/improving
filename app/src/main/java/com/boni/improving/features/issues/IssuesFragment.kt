package com.boni.improving.features.issues

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.boni.improving.R
import com.boni.improving.data.base.*
import com.boni.improving.features.model.Issue
import com.boni.improving.utils.gone
import com.boni.improving.utils.show
import kotlinx.android.synthetic.main.fragment_issues.*
import org.koin.android.viewmodel.ext.android.viewModel

class IssuesFragment : BaseFragment(), ViewModelInterface<IssuesViewModel> {

    private val issuesViewModel by viewModel<IssuesViewModel>()

    override val viewModel: IssuesViewModel
        get() = issuesViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_issues,
            container,
            false
        )
    }

    override fun renderState(viewState: ViewState?) {
        super.renderState(viewState)

        when (viewState) {
            is IssuesState.IssueList -> renderIssueList(viewState.issues)
            is LoadingState.Show -> shimmer_container.startShimmerAnimation()
            is LoadingState.Hide -> {
                shimmer_container.stopShimmerAnimation()
                shimmer_container.gone()
            }
            is ErrorState -> Toast.makeText(requireContext(), viewState.message, Toast.LENGTH_LONG).show()
        }
    }

    private fun renderIssueList(issueList: List<Issue>) {
        issues.addItemDecoration(DividerItemDecoration(requireContext(), LinearLayout.VERTICAL))
        issues.adapter = IssuesAdapter(issueList) {
            val action = IssuesFragmentDirections.actionIssuesFragmentToDetailFragment(it)
            findNavController().navigate(action)
        }
        issues.show()
    }
}