package com.boni.improving.features.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Issue(
    val title: String,
    val state: String,
    val description: String,
    val avatar: String,
    val createdAt: String,
    val link: String
): Parcelable