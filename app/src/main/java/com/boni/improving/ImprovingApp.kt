package com.boni.improving

import android.app.Application
import com.boni.improving.di.networkModule
import com.boni.improving.di.repositoryModule
import com.boni.improving.di.viewModelModule
import org.koin.android.ext.koin.androidContext

class ImprovingApp: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin()
    }

    private fun startKoin() {
        org.koin.core.context.startKoin {
            androidContext(this@ImprovingApp)
            modules(
                listOf(
                    networkModule,
                    repositoryModule,
                    viewModelModule
                )
            )
        }
    }
}