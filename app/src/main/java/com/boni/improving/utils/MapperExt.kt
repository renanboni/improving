package com.boni.improving.utils

import com.boni.improving.data.model.IssueModel
import com.boni.improving.features.model.Issue

fun IssueModel.toIssue(): Issue {
    return Issue(
        title = title.orEmpty(),
        state = state.orEmpty(),
        description = body.orEmpty(),
        avatar = user?.avatarUrl.orEmpty(),
        createdAt = createdAt.orEmpty(),
        link = htmlUrl.orEmpty()
    )
}

fun List<IssueModel>.toIssueList(): List<Issue> {
    return map { it.toIssue() }
}