package com.boni.improving.di

import com.boni.improving.features.issues.IssuesViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { IssuesViewModel(get()) }
}