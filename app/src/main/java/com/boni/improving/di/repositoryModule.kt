package com.boni.improving.di

import com.boni.improving.data.repository.ImprovingRepository
import com.boni.improving.data.repository.ImprovingRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single<ImprovingRepository> { ImprovingRepositoryImpl(get()) }
}