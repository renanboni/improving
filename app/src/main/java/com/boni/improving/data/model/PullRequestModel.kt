package com.boni.improving.data.model

import com.google.gson.annotations.SerializedName

data class PullRequestModel(
    @SerializedName("diff_url") val diffUrl: String? = null,
    @SerializedName("html_url") val htmlUrl: String? = null,
    @SerializedName("patch_url") val patchUrl: String? = null,
    @SerializedName("url") val url: String? = null
)
