package com.boni.improving.data.model

import com.google.gson.annotations.SerializedName

data class LabelModel(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("node_id") val nodeId: String? = null,
    @SerializedName("url") val url: String? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("color") val color: String? = null,
    @SerializedName("default") val default: Boolean? = null,
    @SerializedName("description") val description: String? = null
)