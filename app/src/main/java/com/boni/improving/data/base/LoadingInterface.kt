package com.boni.improving.data.base

interface LoadingInterface {
    fun showLoading()
    fun hideLoading()
}