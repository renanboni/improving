package com.boni.improving.data.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*

abstract class BaseFragment: Fragment() {

    private val viewModelInterface by lazy { this as? ViewModelInterface<*> }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelInterface?.prepare(this)
    }

    fun setToolbarTitle(text: String) {
        activity?.toolbar?.title = text
    }
}