package com.boni.improving.data.model

import com.google.gson.annotations.SerializedName
import java.util.Date

class MilestoneModel(
    @SerializedName("url") val url: String? = null,
    @SerializedName("html_url") val htmlUrl: String? = null,
    @SerializedName("labels_url") val labelsUrl: String? = null,
    @SerializedName("id") val id: Int? = null,
    @SerializedName("node_id") val nodeId: String? = null,
    @SerializedName("number") val number: Int? = null,
    @SerializedName("title") val title: String? = null,
    @SerializedName("description") val description: String? = null,
    @SerializedName("open_issues") val openIssues: Int? = null,
    @SerializedName("closed_issues") val closedIssues: Int? = null,
    @SerializedName("state") val state: String? = null,
    @SerializedName("created_at") val createdAt: String? = null,
    @SerializedName("updated_at") val updatedAt: String? = null,
    @SerializedName("due_on") val dueOn: Date? = null,
    @SerializedName("closed_at") val closedAt: Date? = null
)