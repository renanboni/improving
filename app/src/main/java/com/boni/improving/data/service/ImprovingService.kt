package com.boni.improving.data.service

import com.boni.improving.data.model.IssueModel
import retrofit2.http.GET

interface ImprovingService {

    @GET("repos/JetBrains/kotlin/issues")
    suspend fun getIssues(): List<IssueModel>
}