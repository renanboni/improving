package com.boni.improving.data.base

interface ViewState

sealed class LoadingState : ViewState {
    object Show : LoadingState()
    object Hide : LoadingState()
}

open class ErrorState(val message: String? = "") : ViewState

