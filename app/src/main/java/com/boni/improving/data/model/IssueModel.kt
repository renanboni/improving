package com.boni.improving.data.model

import com.google.gson.annotations.SerializedName
import java.util.Date

class IssueModel(
    @SerializedName("url") val url: String? = null,
    @SerializedName("repository_url") val repositoryUrl: String? = null,
    @SerializedName("labels_url") val labelsUrl: String? = null,
    @SerializedName("comments_url") val commentsUrl: String? = null,
    @SerializedName("events_url") val eventsUrl: String? = null,
    @SerializedName("html_url") val htmlUrl: String? = null,
    @SerializedName("id") val id: Int? = null,
    @SerializedName("node_id") val nodeId: String? = null,
    @SerializedName("number") val number: Int? = null,
    @SerializedName("title") val title: String? = null,
    @SerializedName("user") val user: UserModel? = null,
    @SerializedName("labels") val labels: List<LabelModel>? = null,
    @SerializedName("state") val state: String? = null,
    @SerializedName("locked") val locked: Boolean? = null,
    @SerializedName("assignee") val assignee: AssigneeModel? = null,
    @SerializedName("assignees") val assignees: List<AssigneeModel>? = null,
    @SerializedName("milestone") val milestone: MilestoneModel? = null,
    @SerializedName("comments") val comments: Int? = null,
    @SerializedName("created_at") val createdAt: String? = null,
    @SerializedName("updated_at") val updatedAt: String? = null,
    @SerializedName("closed_at") val closedAt: Date? = null,
    @SerializedName("author_association") val authorAssociation: String? = null,
    @SerializedName("pull_request") val pullRequest: PullRequestModel? = null,
    @SerializedName("body") val body: String? = null
)