package com.boni.improving.data.repository

import com.boni.improving.data.base.Result
import com.boni.improving.data.model.IssueModel
import com.boni.improving.data.service.ImprovingService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class ImprovingRepositoryImpl(private val service: ImprovingService) : ImprovingRepository {

    private var issueList = listOf<IssueModel>()

    override suspend fun getIssues(): Result<List<IssueModel>> {
        return withContext(Dispatchers.IO) {
            try {
                if (issueList.isEmpty()) {
                    issueList = service.getIssues()
                    Result.Success(issueList)
                } else {
                    Result.Success(issueList)
                }
            } catch (e: Exception) {
                Result.Error(e)
            }
        }
    }
}