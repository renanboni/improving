package com.boni.improving.data.repository

import com.boni.improving.data.base.Result
import com.boni.improving.data.model.IssueModel

interface ImprovingRepository {
    suspend fun getIssues(): Result<List<IssueModel>>
}