package com.boni.improving.data.repository

import com.boni.improving.data.base.Result
import com.boni.improving.data.model.IssueModel
import com.boni.improving.data.service.ImprovingService
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ImprovingRepositoryImplTest {

    @MockK
    private lateinit var service: ImprovingService

    lateinit var repository: ImprovingRepositoryImpl

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        repository = ImprovingRepositoryImpl(service)
    }

    @Test
    fun `when getIssues get called should returns list of issues`() = runBlocking {
        val issues = listOf<IssueModel>()

        coEvery { service.getIssues() } returns issues

        assertEquals(repository.getIssues(), Result.Success(issues))
    }

    @Test
    fun `when getIssues get called thrown an error`() = runBlocking {
        val error = Exception("exception")

        coEvery { service.getIssues() } throws error

        assertEquals(repository.getIssues(), Result.Error(error))
    }
}