package com.boni.improving.features.issues

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.boni.improving.data.base.ErrorState
import com.boni.improving.data.base.LoadingState
import com.boni.improving.data.base.Result
import com.boni.improving.data.base.ViewState
import com.boni.improving.data.model.IssueModel
import com.boni.improving.data.repository.ImprovingRepository
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class IssuesViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var repository: ImprovingRepository

    @Mock
    private lateinit var viewState: Observer<ViewState>

    private lateinit var viewModel: IssuesViewModel

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(TestCoroutineDispatcher())
        viewModel = IssuesViewModel(repository)
        viewModel.viewState.observeForever(viewState)
    }

    @Test
    fun `get issues with success should return list of issues`()  {
        runBlocking {
            val issues = listOf<IssueModel>()

            whenever(repository.getIssues()).thenReturn(Result.Success(issues))

            viewModel.doIssuesRequest()

            argumentCaptor<ViewState> {
                verify(viewState, times(3)).onChanged(capture())

                assertEquals(firstValue, LoadingState.Show)
                assertEquals((secondValue as IssuesState.IssueList).issues, issues)
                assertEquals(thirdValue, LoadingState.Hide)
            }
        }
    }

    @Test
    fun `get issues with success should thrown an error`()  {
        runBlocking {
            val exception = Exception("exception")

            whenever(repository.getIssues()).thenReturn(Result.Error(exception))

            viewModel.doIssuesRequest()

            argumentCaptor<ViewState> {
                verify(viewState, times(3)).onChanged(capture())

                assertEquals(firstValue, LoadingState.Show)
                assertEquals((secondValue as ErrorState).message, exception.localizedMessage)
                assertEquals(thirdValue, LoadingState.Hide)
            }
        }
    }
}